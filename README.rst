Vaultwarden
===========

This component is deploys a roughly standalone application for password management. Vaultwarden is an open source Rust reimplementation of the popular password manager Bitwarden.

Terragrunt Configuration
------------------------

.. literalinclude:: terragrunt.hcl
  :caption: ``terragrunt.hcl``

Terraform Configuration
-----------------------

.. literalinclude:: vaultwarden.tf
  :caption: ``vaultwarden.tf``

.. literalinclude:: postgresql.tf
  :caption: ``postgresql.tf``
