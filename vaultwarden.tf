locals {
  vaultwarden_fqdn = "vaultwarden.${var.domain}"
}

variable "vaultwarden_admin_token" {
  type      = string
  sensitive = true
}

variable "domain" {
  type = string
}

variable "smtp" {
  type = object({
    host     = string
    port     = string
    tls      = bool
    username = string
    password = string
  })
}
variable "email_pattern" {
  type = object({
    from = string
    From = string
  })
  validation {
    condition     = length(regexall("%s", var.email_pattern.from)) == 1
    error_message = "email_pattern from must have exactly one '%s' for substitution"
  }
  validation {
    condition     = length(regexall("%s", var.email_pattern.From)) == 1
    error_message = "email_pattern From must have exactly one '%s' for substitution"
  }
}

variable "certificate_issuer" {
  type = string
}

resource "kubernetes_namespace" "vaultwarden" {
  metadata {
    name = "vaultwarden"
  }
}

resource "helm_release" "vaultwarden" {
  name       = "vaultwarden"
  repository = "https://gissilabs.github.io/charts/"
  chart      = "vaultwarden"
  namespace  = one(kubernetes_namespace.vaultwarden.metadata).name
  version    = "1.0.0"

  # Move sensitive values out to set, so that the values is nonsensitive.
  set {
    name  = "database.url"
    value = local.vaultwarden_db_url
  }
  set {
    name  = "vaultwarden.smtp.password"
    value = var.smtp.password
  }
  set {
    name  = "vaultwarden.admin.token"
    value = var.vaultwarden_admin_token
  }

  values = [
    yamlencode({
      image = {
        tag = "1.26.0"
      }
      database = {
        type = "postgresql"
      }
      vaultwarden = {
        domain           = "https://${local.vaultwarden_fqdn}"
        allowSignups     = false
        allowInvitiation = true
        admin = {
          enabled = true
        }
        smtp = {
          enabled = true
          host    = var.smtp.host
          port    = var.smtp.port
          #          ssl      = var.smtp.tls
          security = var.smtp.tls ? "starttls" : "off"
          user     = var.smtp.username
          from     = replace(var.email_pattern.from, "/%s/", "vaultwarden")
          fromName = replace(var.email_pattern.From, "/%s/", "VaultWarden")
        }

        # https://github.com/dani-garcia/vaultwarden/wiki/Running-without-WAL-enabled
        ENABLE_DB_WAL = "false"
      }

      ingress = {
        enabled = true
        annotations = {
          "cert-manager.io/cluster-issuer" = var.certificate_issuer
          # Override the default annotations
          #"kubernetes.io/ingress.class" = ""
          "kubernetes.io/tls-acme" = ""
        }
        host = local.vaultwarden_fqdn
        tls = [
          {
            secretName = "vaultwarden-tls"
            hosts      = [local.vaultwarden_fqdn]
          }
        ]
      }

      ## Persist data to a persitent volume
      persistence = {
        enabled = true
      }
    })
  ]
}

output "vaultwarden_db_url" {
  sensitive   = true
  description = "Internal DB url for Vaultwarden"
  value       = local.vaultwarden_db_url
}
