locals {
  vaultwarden_db          = "vaultwarden"
  vaultwarden_db_user     = "vaultwarden"
  vaultwarden_db_password = random_password.vaultwarden_db_password.result
  vaultwarden_db_host = join("", [
    one(data.kubernetes_service.vaultwarden_db.metadata).name,
    ".${one(kubernetes_namespace.vaultwarden.metadata).name}.svc:5432",
  ])
  vaultwarden_db_url = join("", [
    "postgresql://",
    "${local.vaultwarden_db_user}:${local.vaultwarden_db_password}",
    "@${local.vaultwarden_db_host}",
    "/${local.vaultwarden_db}",
  ])
}

resource "random_password" "vaultwarden_db_password" {
  length  = 32
  special = false
}

resource "helm_release" "vaultwarden_db" {
  name       = "vaultwarden-db"
  repository = "https://charts.bitnami.com/bitnami"
  chart      = "postgresql"
  namespace  = one(kubernetes_namespace.vaultwarden.metadata).name

  values = [
    yamlencode({
      auth = {
        enablePostgresUser = false
        postgresPassword   = ""
        database           = local.vaultwarden_db
        username           = local.vaultwarden_db_user
        password           = local.vaultwarden_db_password
      }
      primary = {
        persistence = {
          size = "8Gi"
        }
      }
    })
  ]
}

data "kubernetes_service" "vaultwarden_db" {
  depends_on = [helm_release.vaultwarden_db]
  metadata {
    namespace = one(kubernetes_namespace.vaultwarden.metadata).name
    name      = "vaultwarden-db-postgresql"
  }
}
